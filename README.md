# Prepare
docker pull redis:3.2-alpine
docker pull alpine:3.4


# Run apps
docker network create tcounter

docker run -it --name tredis --net=tcounter redis:3.2-alpine
docker run -it --name tcounter --net=tcounter -p 5000:5000 -e REDIS_HOST=tredis test/counter:0.1


docker rm tredis tcounter


Access
http://172.20.20.20:5000/

